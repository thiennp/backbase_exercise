'use strict';

/**
 * Application: backbaseExercise
 * Module: main module
 * --------------------------------------------------------------------------------------------------------------------
 * Main module controller
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * main.controller.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise')
  .controller('MainCtrl', function () {
    var vm = this;
  });
