'use strict';

/**
 * Application: backbaseExercise
 * Module: main module
 * --------------------------------------------------------------------------------------------------------------------
 * Initial configurations of the appplication
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * main.config.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise')

  // States config
  .config(function ($stateProvider, $urlRouterProvider) {

    // Main state
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      });

    // Define default route when user go to wrong url
    $urlRouterProvider
      .otherwise('/');
  })

  // Angular Material config
  .config(function ($mdThemingProvider) {

    // Theme config
    $mdThemingProvider.theme('default')
      .primaryPalette('red')
      .accentPalette('indigo');
  });;