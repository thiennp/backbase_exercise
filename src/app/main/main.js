'use strict';

/**
 * Application: backbaseExercise
 * Module: main module
 * --------------------------------------------------------------------------------------------------------------------
 * Define list of using modules
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * main.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise', [
    // Libraries
    'ui.router',
    'ngMaterial',

    // Components
    'backbaseExercise_header',
    'backbaseExercise_footer',
    'backbaseExercise_event'
  ]);