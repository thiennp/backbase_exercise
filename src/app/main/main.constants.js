'use strict';

/**
 * Application: backbaseExercise
 * Module: main module
 * --------------------------------------------------------------------------------------------------------------------
 * Define list of constants
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * main.constants.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise')
  
  // Data server url
  .constant('_DATA_URL', 'http://citysdk.dmci.hva.nl/CitySDK/');