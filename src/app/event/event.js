'use strict';

/**
 * Application: backbaseExercise
 * Module: backbaseExercise_event
 * --------------------------------------------------------------------------------------------------------------------
 * Define list of using modules
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * event.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_event', [
    'ngResource',
    'ngSanitize',
    'ui.router'
  ]);