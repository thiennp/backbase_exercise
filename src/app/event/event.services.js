'use strict';

/**
 * Application: backbaseExercise
 * Module: backbaseExercise_event
 * --------------------------------------------------------------------------------------------------------------------
 * Communicate with data server
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * event.services.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_event')
  .factory('EventService', function ($resource, _DATA_URL) {

    // function getAllEvents
    // ----------------------------------------------------------------------------------------------------------------
    // No params
    // 
    // Get the list of festival events in Amsterdam
    function getAllEvents() {
      var EventsResource = $resource(_DATA_URL + '/events/search');
      return EventsResource.get({ category: 'festival' }).$promise;
    }

    // function getEvent
    // ----------------------------------------------------------------------------------------------------------------
    // @params:
    // id: String
    // 
    // Get an event's details by its id value
    function getEvent(id) {
      var EventResource = $resource(_DATA_URL + 'pois/' + id);
      return EventResource.get({}).$promise;
    }

    return {
      getAllEvents: getAllEvents,
      getEvent: getEvent
    };
  });
