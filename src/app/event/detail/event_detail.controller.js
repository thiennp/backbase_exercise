'use strict';

/**
 * Application: backbaseExercise
 * Module: backbaseExercise_event
 * --------------------------------------------------------------------------------------------------------------------
 * Event Detail Controller
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * event_detail.controller.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular.module('backbaseExercise_event')
  .controller('EventDetailCtrl', ['$mdDialog', '$timeout', 'EventService', 'event', function ($mdDialog, $timeout, EventService, event) {
    var vm = this;
    vm.event = event;

    // Hardcode default zoom
    var defaultZoom = 10;

    // Data loading state
    vm.loading = true;

    // Data map loading state
    vm.mapLoading = true;

    // function initMap
    // ----------------------------------------------------------------------------------------------------------------
    // No params
    // 
    // Init google map
    vm.initMap = function () {

      // Skip if event has no location
      if (!vm.event.location.point.length) {
        return;
      }

      // List of event locations
      var latLngs = [];
      angular.forEach(vm.event.location.point, function (point, index) {
        var newLatLng = point.Point.posList.split(' ');
        latLngs[index] = [Number(newLatLng[0]), Number(newLatLng[1])];
      });

      // Using timeout to make sure that the DOM element which has id name "map" is already loaded
      // Delay 1000 ms to ensure that the element is expanded to its full width
      $timeout(function () {

        // Map boundary
        var bounds = new google.maps.LatLngBounds();

        // Display the map with the first marker in center
        // ------------------------------------------------------------------------------------------------------------

        // Map center
        var center = { lat: latLngs[0][0], lng: latLngs[0][1] };

        // Init map
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: defaultZoom,
          center: center
        });

        // Add marker
        var marker = new google.maps.Marker({
          position: center,
          map: map
        });

        bounds.extend(center);

        // If there're more than 1 markers, change map's center and zoom to fit them
        // ------------------------------------------------------------------------------------------------------------

        if (latLngs.length > 1) {
          angular.forEach(latLngs, function (latLng, index) {

            // Ignore the first marker
            if (index > 0) {
              var newLatLng = { lat: latLng[0], lng: latLng[1] };

              // Add marker
              var marker = new google.maps.Marker({
                position: newLatLng,
                map: map
              });
              bounds.extend(newLatLng);
              map.fitBounds(bounds);
            }
          });
        }

        vm.mapLoading = false;
      }, 1000);
    };

    // function close
    // No params
    // 
    // Close the popup
    vm.close = function () {
      $mdDialog.cancel();
    };

    // Get event's details
    EventService
      .getEvent(event.location.relationship[0].targetPOI)
      .then(function (response) {
        vm.eventLocation = response;
        vm.loading = false;
      });
  }]);
