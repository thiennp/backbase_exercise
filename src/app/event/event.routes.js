'use strict';

/**
 * Application: backbaseExercise
 * Module: backbaseExercise_event
 * --------------------------------------------------------------------------------------------------------------------
 * Define routes to this module
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * event.routes.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_event')
  .config(function ($stateProvider) {
    $stateProvider
      .state('event', {
        url: '/event',
        templateUrl: 'app/event/list/event_list.html',
        controller: 'EventListCtrl',
        controllerAs: 'vm'
      });
  });
