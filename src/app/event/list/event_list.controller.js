'use strict';

/**
 * Application: backbaseExercise
 * Module: backbaseExercise_event
 * --------------------------------------------------------------------------------------------------------------------
 * Event List Controller
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * event_list.controller.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular.module('backbaseExercise_event')
  .controller('EventListCtrl', ['$mdDialog', 'EventService', function ($mdDialog, EventService) {
    var vm = this;
    vm.eventList = [];

    // Data loading state
    vm.loading = true;

    // Get all events
    EventService
      .getAllEvents()
      .then(function (response) {
        vm.eventList = response.event;
        vm.loading = false;
      });

    // function showEvent
    // ----------------------------------------------------------------------------------------------------------------
    // @params:
    // event: Object
    // 
    // Show single event
    vm.showEvent = function (event) {
      $mdDialog.show({
        controller: 'EventDetailCtrl',
        controllerAs: 'vm',
        templateUrl: 'app/event/detail/event_detail.html',
        clickOutsideToClose: true,
        locals: {
          event: event
        }
      });
    };
  }]);
