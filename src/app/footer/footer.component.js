'use strict';

/**
 * Application: backbaseExercise
 * Module: footer module
 * --------------------------------------------------------------------------------------------------------------------
 * Define footer component
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * footer.component.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_footer')
  .component('footer', {
    templateUrl: 'app/footer/footer.html'
  });