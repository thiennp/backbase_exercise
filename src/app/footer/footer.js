'use strict';

/**
 * Application: backbaseExercise
 * Module: footer module
 * --------------------------------------------------------------------------------------------------------------------
 * Define list of using modules
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * footer.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_footer', []);