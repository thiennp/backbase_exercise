'use strict';

/**
 * Application: backbaseExercise
 * Module: header module
 * --------------------------------------------------------------------------------------------------------------------
 * Define header component
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * header.component.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_header')
  .component('header', {
    templateUrl: 'app/header/header.html'
  });