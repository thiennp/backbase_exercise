'use strict';

/**
 * Application: backbaseExercise
 * Module: header module
 * --------------------------------------------------------------------------------------------------------------------
 * Define list of using modules
 * 
 * --------------------------------------------------------------------------------------------------------------------
 * header.js
 * 
 * Created by Thien Nguyen
 * Oct 20th, 2016
 */

angular
  .module('backbaseExercise_header', []);